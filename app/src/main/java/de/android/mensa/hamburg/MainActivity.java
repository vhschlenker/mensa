package de.android.mensa.hamburg;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ShortcutInfo;
import android.content.pm.ShortcutManager;
import android.graphics.drawable.Icon;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ThemedSpinnerAdapter;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.android.mensa.hamburg.materials.Canteen;
import de.android.mensa.hamburg.settings.SettingsActivity;

import static de.android.mensa.hamburg.MainPresenterImpl.SHORTCUTCOUNT;
import static de.android.mensa.hamburg.MainPresenterImpl.SHORTCUTSCHEME;

public class MainActivity extends AppCompatActivity implements IMainView {

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    @BindView(R.id.container)
    ViewPager mViewPager;
    @BindView(R.id.tabs)
    TabLayout mTabLayout;
    @BindView(R.id.spinner)
    Spinner spinner;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private FragmentStatePagerAdapter mSectionsPagerAdapter;
    private IMainPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setDisplayShowTitleEnabled(false);
        }

        presenter = new MainPresenterImpl(this, PreferenceManager.getDefaultSharedPreferences(this));

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        if ((mViewPager != null) && (mTabLayout != null)) {
            presenter.setupAndRenew();
            mViewPager.setAdapter(mSectionsPagerAdapter);
            mTabLayout.setupWithViewPager(mViewPager);
        }

        // Setup spinner
        String[] day_spinner_strings = getResources().getStringArray(R.array.day_spinner);
        if ((toolbar != null) && (spinner != null)) {
            spinner.setAdapter(new DayAdapter(
                    toolbar.getContext(),
                    day_spinner_strings
            ));
            spinner.setOnItemSelectedListener(new daySpinnerSelectedItemListener());
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                startActivity(new Intent(this, SettingsActivity.class));
                return true;
            case R.id.action_about:
                new AboutDialog(this);
                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        presenter.setupAndRenew();
        presenter.handleIntent(getIntent());
        super.onResume();
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    public Context getActivityContext() {
        return this;
    }

    @Override
    public void showDeviceIsOffline() {
        View parentLayout = findViewById(R.id.main_content);
        if (parentLayout != null) {
            Snackbar.make(parentLayout, R.string.dialog_offline, Snackbar.LENGTH_LONG).show();
        } else {
            Toast.makeText(getApplicationContext(), R.string.dialog_offline, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void notifyCanteensListChanged() {
        mSectionsPagerAdapter.notifyDataSetChanged();
    }

    @Override
    public void updateCurrentItemOnDateChange() {
        mSectionsPagerAdapter.getItem(mViewPager.getCurrentItem());
        notifyCanteensListChanged();
    }

    @Override
    public void setCanteenListScrollable(boolean scrollable) {
        if(scrollable){
            mTabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        } else {
            mTabLayout.setTabMode(TabLayout.MODE_FIXED);
        }
    }

    @Override
    public IMainPresenter getActivityPresenter() {
        return this.presenter;
    }

    @Override
    public void setCurrentItem(int index) {
        mViewPager.setCurrentItem(index);
    }

    @Override
    public void showAlertDialog(String Message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(Message);
        alertDialogBuilder.show();
    }

    @Override
    public NetworkInfo getNetworkInfo() {
        ConnectivityManager connMgr = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        return connMgr.getActiveNetworkInfo();
    }

    @Override
    public void addDynamicShortcuts() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N_MR1) {
            Icon icon = Icon.createWithResource(this, R.drawable.canteen_shortcut_icon);
            ShortcutManager shortcutManager = this.getSystemService(ShortcutManager.class);
            ArrayList<ShortcutInfo> shortcut_list = new ArrayList<>();
            ArrayList<Canteen> canteen_list = presenter.getCanteensForDynamicShortcut(SHORTCUTCOUNT);
            for (int i = 0; (i < SHORTCUTCOUNT) && (i < canteen_list.size()); i++) {
                Canteen canteen = canteen_list.get(i);
                ShortcutInfo shortcut = new ShortcutInfo.Builder(this, "id" + Integer.toString(i))
                        .setShortLabel(canteen.getShortName())
                        .setLongLabel(canteen.getName())
                        .setIcon(icon)
                        .setIntent(new Intent(Intent.ACTION_VIEW,
                                Uri.parse(SHORTCUTSCHEME + "://de.android.mensa.hamburg/" + canteen.getCode())))
                        .build();
                shortcut_list.add(shortcut);
            }
            shortcutManager.setDynamicShortcuts(shortcut_list);
        }
    }

    private class DayAdapter extends ArrayAdapter<String> implements ThemedSpinnerAdapter {
        DayAdapter(Context context, String[] objects) {
            super(context, android.R.layout.simple_list_item_1, objects);
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    private class SectionsPagerAdapter extends FragmentStatePagerAdapter {

        SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a DishListFragmentImpl (defined as a static inner class below).
            return presenter.returnSpecificFragment(position);
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public int getCount() {
            return presenter.getCanteensListSize();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return presenter.getShortNameForPosition(position);
        }
    }

    private class daySpinnerSelectedItemListener implements AdapterView.OnItemSelectedListener {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            presenter.changedDayOnSpinner(position);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }
}
