package de.android.mensa.hamburg.settings;

import android.app.Fragment;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import de.android.mensa.hamburg.R;
import de.android.mensa.hamburg.materials.Canteen;
import de.android.mensa.hamburg.tools.ArrayHelper;

public class MensaListFragment extends Fragment {

    private MyArrayAdapter mAdapter;
    private ListView mListView;
    private boolean mSortable = false;
    private Canteen mDragCanteen;
    private int mPosition = -1;

    private ArrayList<Canteen> canteensList;
    private ArrayHelper arrayhelper;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        arrayhelper = new ArrayHelper(getActivity());
        canteensList = arrayhelper.getArray("canteens", true);
        mAdapter = new MyArrayAdapter(getActivity(), R.layout.row_string, canteensList);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_mensalist, parent, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mListView = (ListView) view.findViewById(R.id.listView);
        mListView.setAdapter(mAdapter);
        mListView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                if (!mSortable) {
                    return false;
                }
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        break;
                    }
                    case MotionEvent.ACTION_MOVE: {
                        int position = mListView.pointToPosition((int) event.getX(), (int) event.getY());
                        if (position < 0) {
                            break;
                        }
                        if (position != mPosition) {
                            mPosition = position;
                            mAdapter.remove(mDragCanteen);
                            mAdapter.insert(mDragCanteen, mPosition);
                        }
                        return true;
                    }
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_CANCEL:
                    case MotionEvent.ACTION_OUTSIDE: {
                        stopDrag();
                        return true;
                    }
                }
                return false;
            }
        });
    }

    @Override
    public void onDestroy() {
        arrayhelper.saveArray("canteens", canteensList);
        super.onDestroy();
    }

    private void startDrag(Canteen canteen) {
        mPosition = -1;
        mSortable = true;
        mDragCanteen = canteen;
        mAdapter.notifyDataSetChanged();
    }

    private void stopDrag() {
        mPosition = -1;
        mSortable = false;
        mDragCanteen = null;
        mAdapter.notifyDataSetChanged();
    }

    private static class ViewHolder {
        CheckBox checkBox;
        TextView title;
        ImageView imageview;
    }

    private class MyArrayAdapter extends ArrayAdapter<Canteen> {
        private final ArrayList<Canteen> mList;
        private final LayoutInflater mInflater;
        private final int mLayout;

        MyArrayAdapter(Context context, int textViewResourceId, ArrayList<Canteen> list) {
            super(context, textViewResourceId, list);
            this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            this.mLayout = textViewResourceId;
            this.mList = list;
        }

        @Override
        public void add(Canteen canteen) {
            mList.add(canteen);
        }

        @Override
        public void insert(Canteen canteen, int position) {
            mList.add(position, canteen);
        }

        @Override
        public void remove(Canteen canteen) {
            super.remove(canteen);
            mList.remove(canteen);
        }

        @Override
        public void clear() {
            super.clear();
            mList.clear();
        }


        @Override
        public View getView(final int position, View view, ViewGroup parent) {
            ViewHolder holder;

            if (view == null) {
                view = mInflater.inflate(this.mLayout, parent, false);
                assert view != null;
                holder = new ViewHolder();
                holder.checkBox = (CheckBox) view.findViewById(R.id.checkbox);
                holder.title = (TextView) view.findViewById(R.id.title);
                holder.imageview = (ImageView) view.findViewById(R.id.imageView);
                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }
            //Set the Tag for the checkbox to remember its state
            holder.checkBox.setTag(position);

            final Canteen canteen = mList.get(position);

            holder.title.setText(canteen.getName());

            holder.checkBox.setChecked(canteen.getVisibility());

            holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    //As long the object is in drag, there should be not boxes be checked
                    if (!mSortable) {
                        //get the tag for the checkbox and switch the appropriate one
                        int getPosition = (Integer) buttonView.getTag();
                        mList.get(getPosition).setVisibility(isChecked);
                    }
                }
            });

            holder.imageview.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                        startDrag(canteen);
                        return true;
                    }
                    return false;
                }
            });

            if (mDragCanteen != null && mDragCanteen.equals(canteen)) {
                view.setBackgroundColor(Color.LTGRAY);
            } else {
                view.setBackgroundColor(Color.TRANSPARENT);
            }

            return view;
        }
    }
}
