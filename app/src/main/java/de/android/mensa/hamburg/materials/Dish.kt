package de.android.mensa.hamburg.materials
import java.util.*

class Dish(val description: String, val priceStudentsString: String, val priceAttendantsString: String, val priceGuestsString: String) {
    val priceStudents: Int
    val priceAttendants: Int
    val priceGuests: Int
    private val _allergens: MutableList<Allergenic> = ArrayList()

    val allergens: List<Allergenic>
        get() = _allergens

    init {
        priceStudents = savePrice(priceStudentsString)
        priceAttendants = savePrice(priceAttendantsString)
        priceGuests = savePrice(priceGuestsString)
    }

    constructor(description: String, price: String) : this(description, price, price, price)


    /**
     * Parse the price string as integer in eurocents
     * We currently know only of this format, everything else shouldn't be parsed
     *
     * @param price A string in the format "1,50 €"
     * @return the price from the string in eurocents
     */
    private fun savePrice(price: String): Int {
        return if (price.matches("\\d+,\\d{0,2}\\s+€".toRegex())) {
            Integer.parseInt(price.replace("[,\\s+€]".toRegex(), ""))
        } else {
            0
        }
    }

    fun addAllergenic(allergenic: Allergenic) {
        if (!_allergens.contains(allergenic)) {
            _allergens.add(allergenic)
        }
    }

    override fun equals(other: Any?): Boolean {
        //If the other Object is null, it's for sure not equal
        if (other == null) {
            return false
        }
        //If the other object is neither a class nora subclass from this one, it is not equal
        if (!Dish::class.java.isAssignableFrom(other.javaClass)) {
            return false
        }
        //Since we know, that we have the same class, we can cast
        val tmp = other as Dish
        //Onto checking the fields for equality
        return (this.description == tmp.description
                && this.priceAttendants == tmp.priceAttendants
                && this.priceAttendantsString == tmp.priceAttendantsString
                && this.priceStudents == tmp.priceStudents
                && this.priceStudentsString == tmp.priceStudentsString
                && this.priceGuests == tmp.priceGuests
                && this.priceGuestsString == tmp.priceGuestsString
                && this.allergens == tmp.allergens)
    }

    override fun hashCode(): Int {
        var hashCode = 42
        val multi = 29
        hashCode += multi * this.description.hashCode()
        hashCode += multi * priceAttendants
        hashCode += multi * priceAttendantsString.hashCode()
        hashCode += multi * priceStudents
        hashCode += multi * priceStudentsString.hashCode()
        hashCode += multi * priceGuests
        hashCode += multi * priceGuestsString.hashCode()
        hashCode += multi * _allergens.hashCode()
        return hashCode
    }
}