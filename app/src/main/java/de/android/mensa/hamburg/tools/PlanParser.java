package de.android.mensa.hamburg.tools;

import android.os.AsyncTask;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import de.android.mensa.hamburg.R;
import de.android.mensa.hamburg.materials.Allergenic;
import de.android.mensa.hamburg.materials.Dish;

public class PlanParser extends AsyncTask<Void, Void, Document> {

    private static final String TAG = "PlanParser";
    private final IPlanParserCallback _planParserCallback;
    private final String _url;

    public PlanParser(int day, String mensa_code, String lang, IPlanParserCallback planParserCallback) {
        int year = Calendar.getInstance().get(Calendar.YEAR);
        _planParserCallback = planParserCallback;
        _url = createURL(day, year, mensa_code, lang);
    }

    @Override
    protected Document doInBackground(Void... params) {
        try {
            return Jsoup.connect(_url).get();
        } catch (IOException e) {
            if (!isCancelled()) {
                _planParserCallback.ErrorWhileFetching();
            }
            return new Document("Unable to retrieve web page. URL may be invalid.");
        }
    }

    @Override
    protected void onPostExecute(Document result) {
        if (!isCancelled()) {
            ArrayList<Dish> dishList = ParseResult(result);
            if (dishList.size() == 0) {
                _planParserCallback.NoMenu();
            } else {
                _planParserCallback.SetDishes(dishList);
            }
        }
    }

    public static String createURL(int day, int year, String mensaCode, String lang) {
        String dayURLPart;
        if (day == 0) {
            dayURLPart = "0";
        } else {
            dayURLPart = "99";
        }

        return "http://speiseplan.studierendenwerk-hamburg.de/" + lang + "/" + mensaCode + "/" + year + "/" + dayURLPart + "/";
    }

    public static String removeBracketsAndRedundantWhitespace(String input) {
        String RemovedBracketsAndTheirContents = input.replaceAll("\\([^)]*\\)", "");
        String RemovedRedundentWhitespace = RemovedBracketsAndTheirContents.replaceAll("\\s{2,}", "");
        return RemovedRedundentWhitespace.replaceAll("\\s*\\Z", "");
    }

    public static ArrayList<Dish> ParseResult(Document result) {
        Elements dishes = result.select("td.dish-description");
        Elements prices = result.select("td.price");

        int i = 0;
        ArrayList<Dish> dishList = new ArrayList<>();
        for (Element dish : dishes) {
            String dishString = removeBracketsAndRedundantWhitespace(dish.text());
            String price_student = prices.get(i).text();
            i++;
            String price_attendants = prices.get(i).text();
            i++;
            String price_guests = prices.get(i).text();
            i++;
            Dish d = new Dish(dishString, price_student, price_attendants, price_guests);

            Elements allergens = dish.select("span.tooltip");
            Elements allergenicNumbers = dish.select("span");

            for (int j = 0; j < allergens.size(); j++) {
                int allergenicNumber = Integer.parseInt(allergenicNumbers.get(j).text());
                String description = parseAllergenicElementTitle(allergens.get(j));
                d.addAllergenic(addAllergenic(allergenicNumber, description));
            }

            dishList.add(d);
        }
        return dishList;
    }

    /**
     * The title of one allergen is for example "Glutenhaltiges Getreide"
     * But it is displayed as "title=Glutenhaltiges Getreide"
     * So accessing the value of the title returns only "Glutenhaltiges"
     * @param allergenic The allergenic tooltip which should be parsed
     * @return A well formatted allergenic description with a complete title
     */
    public static String parseAllergenicElementTitle(Element allergenic){
        StringBuilder descriptionBuilder = new StringBuilder(allergenic.attr("title"));
        List<Attribute> allergenicAttributesList = allergenic.attributes().asList();
        //Remove the first attributes, e.g. class
        // and title and use only the next.
        // Their keys are the missing words from the allergenic
        List<Attribute> missingWords =  allergenicAttributesList.subList(2, allergenicAttributesList.size());
        for (Attribute attribute: missingWords) {
            descriptionBuilder.append(" ");
            descriptionBuilder.append(attribute.getKey());
        }
        return descriptionBuilder.toString();
    }

    /**
     * Adds the allergenic and an icon if avaidable
     * See studierendenwerk-hamburg.de/studierendenwerk/de/essen/lebensmittelinformationen/Symbole-und-Kennzeichen-im-Speiseplan/symbole-und-kennzeichen.php
     * for the Full List
     * TODO: 15 is not entirely accurate, according to the List they are crustaceans(/Krebstiere und Krebstiererzeugnisse )
     * TODO: 27 is not entirely accurate, according to the List they are molluscs(/Weichtiere (z.B. Muscheln und Weinbergschnecken))
     *
     * @param number      The number of the Allergen according to the site
     * @param description The description or title
     * @return An Allergenic Object
     */
    private static Allergenic addAllergenic(int number, String description) {
        int iconId = 0;
        switch (number) {
            case 14:
                iconId = R.drawable.wheat;
                break;
            case 15:
                iconId = R.drawable.shrimp;
                break;
            case 16:
                iconId = R.drawable.eggs;
                break;
            case 17:
                iconId = R.drawable.fish;
                break;
            case 18:
                iconId = R.drawable.peanuts;
                break;
            case 19:
                iconId = R.drawable.soy;
                break;
            case 20:
                iconId = R.drawable.milk;
                break;
            case 21:
                iconId = R.drawable.nuts;
                break;
            case 27:
                iconId = R.drawable.shellfish;
                break;

        }
        if (iconId != 0) {
            return new Allergenic(description, number, iconId);
        } else {
            return new Allergenic(description, number);
        }
    }
}
