package de.android.mensa.hamburg;

import java.util.List;

import de.android.mensa.hamburg.materials.Allergenic;

interface IDishAdapter {
    /**
     * Set any available icons for the row
     * @param holder The holder where the icons should be set
     * @param position The position of the connected dish
     * @return Have icons been set (Some allergens don't have icons, so none are set)
     */
    boolean setIconsIfAvailable(DishAdapterImpl.ViewHolder holder, int position);

    /**
     * Handles the request of the onlick method to create a dialog,
     * or not, if there are nor allergens to show.
     *
     * @param itemposition the position of the item which was clicked
     */
    void handleDialogRequest(int itemposition);

    /**
     * A helper method which uses the stringbuilder to
     * quickly create a list of the strings with linebreaks.
     * We assume, that allergens is not null, since it is checked by handleDialogRequest
     * which is the only caller method.
     *
     * @param allergens the list of allergens, from which the strings a generated
     * @return the list of strings with linebreaks
     */
    String setAllergensString(List<Allergenic> allergens);
}
