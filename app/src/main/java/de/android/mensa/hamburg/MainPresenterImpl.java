package de.android.mensa.hamburg;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.util.Log;

import java.util.ArrayList;

import de.android.mensa.hamburg.materials.Canteen;
import de.android.mensa.hamburg.tools.ArrayHelper;

class MainPresenterImpl implements IMainPresenter {

    private static final String TAG = "MainPresenterImpl";
    static final String SHORTCUTSCHEME = "shortcut";
    static final int SHORTCUTCOUNT = 3;
    /**
     * Current scheme:
     * 0 Today
     * 1 Tomorrow
     */
    private int _day = 0;
    private String _lang;
    private ArrayList<Canteen> canteens_list;
    private ArrayHelper arrayhelper;
    private SharedPreferences sPrefs;
    private boolean showStudentPrice;
    private boolean showAttendantPrice;
    private boolean showGuestPrice;
    private boolean showAllergensIcon;
    private IMainView mainView;

    MainPresenterImpl(IMainView view, SharedPreferences sPrefs) {
        this.mainView = view;
        this.sPrefs = sPrefs;
    }


    @Override
    public void setupAndRenew() {
        arrayhelper = new ArrayHelper(mainView.getAppContext());
        canteens_list = arrayhelper.getArray("canteens", false);
        mainView.notifyCanteensListChanged();
        if (canteens_list.size() > 5) {
            mainView.setCanteenListScrollable(true);
        } else {
            mainView.setCanteenListScrollable(false);
        }
        showStudentPrice = sPrefs.getBoolean("student_switch", true);
        showAttendantPrice = sPrefs.getBoolean("attendant_switch", true);
        showGuestPrice = sPrefs.getBoolean("guest_switch", false);
        showAllergensIcon = sPrefs.getBoolean("allergens_switch", true);
        if (sPrefs.getBoolean("language_switch", true)) {
            _lang = "de";
        } else {
            _lang = "en";
        }
        isNetworkEnabled();
        mainView.addDynamicShortcuts();
    }

    @Override
    public boolean isNetworkEnabled() {
        NetworkInfo networkInfo = mainView.getNetworkInfo();
        if (networkInfo == null || !networkInfo.isConnected()) {
            mainView.showDeviceIsOffline();
            return false;
        } else {
            return true;
        }
    }

    @Override
    public IMainView getView() {
        return mainView;
    }

    @Override
    public boolean isShowStudentPrice() {
        return showStudentPrice;
    }

    @Override
    public boolean isShowAttendantPrice() {
        return showAttendantPrice;
    }

    @Override
    public boolean isShowGuestPrice() {
        return showGuestPrice;
    }

    @Override
    public boolean isShownAllergens() {
        return showAllergensIcon;
    }

    @Override
    public int getCanteensListSize() {
        return canteens_list.size();
    }

    @Override
    public CharSequence getShortNameForPosition(int position) {
        return canteens_list.get(position).getShortName();
    }

    @Override
    public Fragment returnSpecificFragment(int position) {
        isNetworkEnabled();
        return DishListFragmentImpl.newInstance(_day, canteens_list.get(position).getCode(), _lang);
    }

    @Override
    public void changedDayOnSpinner(int day) {
        if (isNetworkEnabled()) {
            this._day = day;
            if (getCanteensListSize() != 0) {
                mainView.updateCurrentItemOnDateChange();
            }
        }
    }

    @Override
    public void handleIntent(Intent intent) {
        Uri data = intent.getData();
        if (data == null) {
            return;
        }
        String code;
        if (data.getScheme().equals(SHORTCUTSCHEME)) {
            code = data.getPathSegments().get(0);
        } else {
            code = data.getPathSegments().get(5);
        }
        // We need to clear the intent, otherwise the mensa in the intent will be always added
        // if the user comes back to the MainActivity,
        // even if it was removed in the settings
        intent.setData(null);
        setAndOpenCanteenPerID(code);
    }

    @Override
    public ArrayList<Canteen> getCanteensForDynamicShortcut(int number) {
        return new ArrayList<>(canteens_list.subList(0,number));
    }

    @Override
    public void setAndOpenCanteenPerID(String code) {
        int indexOfCanteenWithThisCode = arrayhelper.getIdOfCanteenWithCode(canteens_list, code);
        if (indexOfCanteenWithThisCode >= 0) {
            mainView.setCurrentItem(indexOfCanteenWithThisCode);
        } else {
            addCanteenToViewPager(code);
        }
    }

    @Override
    public void addCanteenToViewPager(String code) {
        arrayhelper.setVisibilityOfCanteen("canteens", code, true);
        setupAndRenew();
        int indexOfCanteenWithThisCode = arrayhelper.getIdOfCanteenWithCode(canteens_list, code);
        if (indexOfCanteenWithThisCode >= 0) {
            mainView.setCurrentItem(indexOfCanteenWithThisCode);
        } else {
            // This should REALLY never happen.
            Log.e(TAG, "Canteen was added, but not found again. Sorry!");
        }
    }

}
